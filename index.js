const express = require("express");
const {appendFileSync} = require("fs");
const morgan = require("morgan");
const path = require("path");
const {join} = require("path");
const api = require("./routes/api.js");
const web = require("./routes/web.js");

const app = express();
const port = 3000;

// Load static file
app.use(express.static(path.join(__dirname, "public")));

// Set view engine
app.set("view engine", "ejs");

// 3rd party middleware for logging
app.use(morgan("dev"));

// Load routes
app.use("/api", api);
app.use("/", web);

// Internal server error handler middleware
app.use(function (err, req, res, next) {
	res.status(500).json({
		status: "fail",
		errors: err.message,
	});
});

// 404 handler middleware
app.use(function (req, res, next) {
	res.render(join(__dirname, "./views/404"));
});

// Run
app.listen(port, () => {
	console.log("Server berhasil dijalankan");
});
