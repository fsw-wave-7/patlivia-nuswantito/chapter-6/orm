const {User} = require("../../models");
const {Op} = require("sequelize");
class UserController {
	getUser = (req, res) => {
		// SEQUELIZE CODE
		User.findAll().then((user) => {
			console.log(user);
			res.result(user);
		});
	};

	getDetailUser = (req, res) => {
		const index = req.params.index;
		// SEQUELIZE CODE
		User.findAll({
			where: {id: index},
		}).then((user) => {
			console.log(user);
			res.result(user);
		});
	};

	insertUser = (req, res) => {
		const body = req.body;
		// SEQUELIZE CODE
		User.create({
			name: body.name,
			username: body.username,
			age: body.age,
			password: body.password,
			createdAt: new Date(),
			updatedAt: new Date(),
		})
			.then((user) => {
				console.log(user);
				res.result(user);
			})
			.catch((err) => {
				res.status(422).json(`can't create user`);
			});
	};

	updateUser = (req, res) => {
		User.update({
			name: "Kitty",
			password: "meow",
		}).then((user) => {
			console.log(user);
			res.result(user);
		});
	};

	deleteUser = (req, res) => {};
}

module.exports = UserController;
