const {Router} = require("express");
const {join} = require("path");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const AuthController = require("../controllers/web/AuthController");
const HomeController = require("../controllers/web/HomeController");

const web = Router();
web.use(cookieParser());
web.use(bodyParser.json());
web.use(bodyParser.urlencoded({extended: true}));
const authController = new AuthController();
const homeController = new HomeController();

// Menampilkan halaman Login
web.get("/login", authController.login);
web.post("/login", authController.doLogin);
web.get("/logout", authController.logout);
web.use(AuthMiddleware);

web.get("/home", (req, res) => {
	res.render("../views/index");
});

web.get("/add", homeController.add);
web.get("/", homeController.index);

// Menampilkan halaman Form
web.get("/form", (req, res) => {
	res.render("../views/form");
});

// Menampilkan halaman Table
web.get("/table", (req, res) => {
	res.render("../views/table");
});

web.post("/save-user", homeController.saveUser);

web.get("/edit/:id", homeController.editUser);
web.post("/edit/:id", homeController.updateUser);
web.delete("/delete/:id", homeController.deleteUser);
module.exports = web;
